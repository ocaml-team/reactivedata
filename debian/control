Source: reactivedata
Section: ocaml
Priority: optional
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders:
 Stéphane Glondu <glondu@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 libreact-ocaml-dev (>= 1.2.1),
 ocaml,
 ocaml-dune,
 dh-ocaml (>= 1.2)
Standards-Version: 4.5.0
Rules-Requires-Root: no
Homepage: https://github.com/ocsigen/reactiveData
Vcs-Browser: https://salsa.debian.org/ocaml-team/reactivedata
Vcs-Git: https://salsa.debian.org/ocaml-team/reactivedata.git

Package: libreactivedata-ocaml-dev
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 ${ocaml:Depends}
Provides:
 ${ocaml:Provides}
Recommends: ocaml-findlib
Description: FRP with incremental changes in data structures
 ReactiveData is an OCaml module for functional reactive programming
 (FRP) based on React. It adds support to incremental changes in data
 structures by reasoning on patches instead of absolute values.
 .
 This package contains development files of ReactiveData.

Package: libreactivedata-ocaml
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 ${ocaml:Depends}
Provides:
 ${ocaml:Provides}
Description: FRP with incremental changes in data structures (runtime)
 ReactiveData is an OCaml module for functional reactive programming
 (FRP) based on React. It adds support to incremental changes in data
 structures by reasoning on patches instead of absolute values.
 .
 This package contains dynamically loadable plugins of ReactiveData.
